"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.generateRefreshToken = exports.generateAccessToken = exports.hashToken = exports.generateTokens = void 0;
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const crypto_1 = __importDefault(require("crypto"));
function generateAccessToken(user) {
    const payload = {
        email: user.email,
        role: user.role,
        userId: user.id,
        schoolId: user.schoolId,
    };
    return jsonwebtoken_1.default.sign(payload, process.env.JWT_ACCESS_SECRET, {
        expiresIn: '7d',
    });
}
exports.generateAccessToken = generateAccessToken;
function generateRefreshToken(user, jti) {
    const payload = {
        email: user.email,
        role: user.role,
        userId: user.id,
        schoolId: user.schoolId,
        jti,
    };
    return jsonwebtoken_1.default.sign(payload, process.env.JWT_REFRESH_SECRET, {
        expiresIn: '12h',
    });
}
exports.generateRefreshToken = generateRefreshToken;
function generateTokens(user, jti) {
    const accessToken = generateAccessToken(user);
    const refreshToken = generateRefreshToken(user, jti);
    return {
        accessToken,
        refreshToken,
    };
}
exports.generateTokens = generateTokens;
function hashToken(token) {
    return crypto_1.default.createHash('sha512').update(token).digest('hex');
}
exports.hashToken = hashToken;
