"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const auth_1 = require("../middlewares/auth");
const configs_1 = require("../configs");
const notice_1 = require("../services/notice");
const router = (0, express_1.Router)();
router.get('/notice/:id', auth_1.isAuth, async (req, res, next) => {
    try {
        const payload = req.payload;
        const id = req.params.id;
        if (payload.email !== configs_1.SUPER_ADMIN_EMAIL) {
            res.status(401);
            throw new Error('Unauthorized');
        }
        const notice = await (0, notice_1.findNoticeById)(+id);
        if (notice) {
            res.json(notice);
        }
        else {
            res.status(404);
            throw new Error('Notice not found');
        }
    }
    catch (error) {
        next(error);
    }
});
router.get('/notices', auth_1.isAuth, async (req, res, next) => {
    try {
        const payload = req.payload;
        if (payload.email !== configs_1.SUPER_ADMIN_EMAIL) {
            res.status(401);
            throw new Error('Unauthorized');
        }
        const notices = await (0, notice_1.findAllNotices)();
        if (notices) {
            res.json(notices);
        }
        else {
            res.status(404);
            throw new Error('Notices not found');
        }
    }
    catch (error) {
        next(error);
    }
});
router.post('/notice', auth_1.isAuth, async (req, res, next) => {
    try {
        const payload = req.payload;
        if (payload.email !== configs_1.SUPER_ADMIN_EMAIL) {
            res.status(401);
            throw new Error('Unauthorized');
        }
        const notice = req.body;
        const newNotice = await (0, notice_1.createNotice)(notice);
        res.json(newNotice);
    }
    catch (error) {
        next(error);
    }
});
router.put('/notice/:id', auth_1.isAuth, async (req, res, next) => {
    try {
        const payload = req.payload;
        if (payload.email !== configs_1.SUPER_ADMIN_EMAIL) {
            res.status(401);
            throw new Error('Unauthorized');
        }
        const id = req.params.id;
        const notice = req.body;
        const newNotice = await (0, notice_1.updateNoticeById)(+id, notice);
        if (newNotice) {
            res.json(newNotice);
        }
        else {
            res.status(404);
            throw new Error('Notice Update Failed');
        }
    }
    catch (error) {
        next(error);
    }
});
router.delete('/notice/:id', auth_1.isAuth, async (req, res, next) => {
    try {
        const payload = req.payload;
        if (payload.email !== configs_1.SUPER_ADMIN_EMAIL) {
            res.status(401).json({ message: 'Unauthorized' });
        }
        const id = req.params.id;
        const notice = await (0, notice_1.deleteNoticeById)(+id);
        res.json(notice);
    }
    catch (error) {
        next(error);
    }
});
