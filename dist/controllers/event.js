"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const auth_1 = require("../middlewares/auth");
const event_1 = require("../services/event");
const client_1 = require("@prisma/client");
const teacher_1 = require("../services/teacher");
const student_1 = require("../services/student");
const router = (0, express_1.Router)();
/**
 * GET /event/:id
 *
 * @summary get a event
 * @param {number} id the id fo the event
 * @returns {object} 200 - success response
 * @returns {object} 401 - Unauthorized response
 * @example response - 200 - success response example
 *   {
 *     "_id": "Bury the light",
 *     "name": "lorem ipsum",
 *   }
 */
router.get('/event/:id', auth_1.isAuth, async (req, res, next) => {
    try {
        const id = req.params.id;
        const event = await (0, event_1.findEventById)(+id);
        res.json(event);
    }
    catch (error) {
        next(error);
    }
});
/**
 * Short Comment for the function
 * @Summary Get all events
 */
router.get('/events', auth_1.isAuth, async (req, res, next) => {
    try {
        const payload = req.payload;
        switch (payload.role) {
            case client_1.RoleEnumType.teacher:
                const teacher = await (0, teacher_1.findTeacherByUserId)(payload.userId);
                const eventsTeacher = await (0, event_1.findEventByTeacherId)(teacher.id);
                res.json(eventsTeacher);
                break;
            case client_1.RoleEnumType.student:
                const student = await (0, student_1.findStudentByUserId)(payload.userId);
                const eventsStudent = await (0, event_1.findEventByStudentId)(student.id);
                res.json(eventsStudent);
                break;
            case client_1.RoleEnumType.admin:
                const events = await (0, event_1.findAllEvents)();
                res.json(events);
                break;
            default:
                res.status(401);
                throw new Error('User Unauthorized');
        }
    }
    catch (error) {
        next(error);
    }
});
/**
 * Short Comment for the function
 * @Summary Create a new event
 */
router.post('/event', auth_1.isAuth, async (req, res, next) => {
    try {
        const payload = req.payload;
        // Input Validation
        const { title, start, end, allDay, url, className, backgroundColor, borderColor, textColor, image, description, subjectId, teacherId, studentId, } = req.body;
        if (!title || !start || !end || !allDay || !subjectId || !studentId) {
            res.status(400);
            throw new Error('Invalid input');
        }
        switch (payload.role) {
            case client_1.RoleEnumType.admin:
                if (!teacherId) {
                    res.status(400);
                    throw new Error('Invalid input teacherId');
                }
                const eventData = {
                    title,
                    start,
                    end,
                    allDay,
                    url,
                    className,
                    backgroundColor,
                    borderColor,
                    textColor,
                    image,
                    description,
                    subjectId,
                    teacherId,
                    studentId,
                    schoolId: payload.schoolId,
                };
                const eventByAdmin = await (0, event_1.createEvent)(eventData);
                res.json(eventByAdmin);
                break;
            case client_1.RoleEnumType.teacher:
                const teacher = await (0, teacher_1.findTeacherByUserId)(payload.userId);
                const eventDataTeacher = {
                    title,
                    start,
                    end,
                    allDay,
                    url,
                    className,
                    backgroundColor,
                    borderColor,
                    textColor,
                    image,
                    description,
                    subjectId,
                    teacherId: teacher.id,
                    studentId,
                    schoolId: payload.schoolId,
                };
                const eventByTeacher = await (0, event_1.createEvent)(eventDataTeacher);
                res.json(eventByTeacher);
                break;
            default:
                res.status(401);
                throw new Error('User Unauthorized');
        }
    }
    catch (error) {
        next(error);
    }
});
/**
 * Short Comment for the function
 * @Summary  Update a event
 */
router.put('/event/:id', auth_1.isAuth, async (req, res, next) => {
    try {
        const payload = req.payload;
        const id = req.params.id;
        const { title, start, end, allDay, url, className, backgroundColor, borderColor, textColor, image, description, subjectId, teacherId, studentId, } = req.body;
        if (!title || !start || !end || !allDay || !subjectId || !studentId || !id) {
            res.status(400);
            throw new Error('Missing or invalid input');
            return;
        }
        switch (payload.role) {
            case client_1.RoleEnumType.admin:
                if (!teacherId) {
                    res.status(400);
                    throw new Error('input teacherId');
                }
                const eventData = {
                    id: +id,
                    title,
                    start,
                    end,
                    allDay,
                    url,
                    className,
                    backgroundColor,
                    borderColor,
                    textColor,
                    image,
                    description,
                    subjectId,
                    teacherId,
                    studentId,
                    schoolId: payload.schoolId,
                };
                const eventByAdmin = await (0, event_1.createEvent)(eventData);
                res.json(eventByAdmin);
                break;
            case client_1.RoleEnumType.teacher:
                const teacher = await (0, teacher_1.findTeacherByUserId)(payload.userId);
                const eventDataTeacher = {
                    id: +id,
                    title,
                    start,
                    end,
                    allDay,
                    url,
                    className,
                    backgroundColor,
                    borderColor,
                    textColor,
                    image,
                    description,
                    subjectId,
                    teacherId: teacher.id,
                    studentId,
                    schoolId: payload.schoolId,
                };
                const eventByTeacher = await (0, event_1.updateEvent)(eventDataTeacher);
                res.json(eventByTeacher);
        }
    }
    catch (error) {
        next(error);
    }
});
/**
 * Short Comment for the function
 * @Summary Delete a event
 */
router.delete('/event/:id', auth_1.isAuth, async (req, res, next) => {
    try {
        const payload = req.payload;
        const id = req.params.id;
        switch (payload.role) {
            case client_1.RoleEnumType.admin:
                const eventByAdmin = await (0, event_1.deleteEventById)(+id);
                res.json(eventByAdmin);
                break;
            case client_1.RoleEnumType.teacher:
                const eventByTeacher = await (0, event_1.deleteEventById)(+id);
                res.json(eventByTeacher);
                break;
            default:
                res.status(401);
                throw new Error('User Unauthorized');
        }
    }
    catch (error) {
        next(error);
    }
});
exports.default = router;
