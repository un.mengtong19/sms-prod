"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const client_1 = require("@prisma/client");
const express_1 = require("express");
const auth_1 = require("../middlewares/auth");
const configs_1 = require("../configs");
const payment_1 = require("../services/payment");
const router = (0, express_1.Router)();
router.get('/payment/:id', auth_1.isAuth, async (req, res, next) => {
    try {
        const payload = req.payload;
        if (payload.role !== client_1.RoleEnumType.admin) {
            res.status(401);
            throw new Error('Unauthorized');
        }
        const { id } = req.params;
        const payment = (0, payment_1.findPaymentByIdAndSchoolId)(+id, payload.schoolId);
        res.json(payment);
    }
    catch (error) {
        next(error);
    }
});
router.get('/payments', auth_1.isAuth, async (req, res, next) => {
    try {
        const payload = req.payload;
        if (payload.role !== client_1.RoleEnumType.admin) {
            res.status(401);
            throw new Error('Unauthorized');
        }
        const payments = await (0, payment_1.findPaymentsBySchoolId)(+payload.schoolId);
        res.json(payments);
    }
    catch (error) {
        next(error);
    }
});
router.post('/payment', auth_1.isAuth, async (req, res, next) => {
    try {
        const payload = req.payload;
        if (payload.role !== client_1.RoleEnumType.admin) {
            res.status(401);
            throw new Error('Unauthorized');
        }
        const { amount, schoolId, date, studentId, type, description, other, teacherId } = req.body;
        const data = { schoolId, date, studentId, amount, type, description, other, teacherId };
        const payment = await (0, payment_1.createPayment)(data);
        res.json(payment);
    }
    catch (error) {
        next(error);
    }
});
router.put('/payment/:id', auth_1.isAuth, async (req, res, next) => {
    try {
        const payload = req.payload;
        if (payload.role !== client_1.RoleEnumType.admin && payload.email !== configs_1.SUPER_ADMIN_EMAIL) {
            res.status(401);
            throw new Error('Unauthorized');
        }
        const { id } = req.params;
        const { amount, schoolId, date, studentId, teacherId, type, description } = req.body;
        const data = { schoolId, date, studentId, teacherId, amount, type, description };
        const payment = await (0, payment_1.updatePaymentById)(+id, data);
        res.json(payment);
    }
    catch (error) {
        next(error);
    }
});
router.delete('/payment/:id', auth_1.isAuth, async (req, res, next) => {
    try {
        const payload = req.payload;
        if (payload.role !== client_1.RoleEnumType.admin && payload.email !== configs_1.SUPER_ADMIN_EMAIL) {
            res.status(401);
            throw new Error('Unauthorized');
        }
        const { id } = req.params;
        const payment = await (0, payment_1.deletePaymentById)(+id);
        res.json(payment);
    }
    catch (error) {
        next(error);
    }
});
exports.default = router;
