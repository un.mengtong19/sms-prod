"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const client_1 = require("@prisma/client");
const express_1 = require("express");
const auth_1 = require("../middlewares/auth");
const configs_1 = require("../configs");
const expense_1 = require("../services/expense");
const router = (0, express_1.Router)();
router.get('/expense/:id', auth_1.isAuth, async (req, res, next) => {
    try {
        const payload = req.payload;
        if (payload.role !== client_1.RoleEnumType.admin) {
            res.status(401);
            throw new Error('Unauthorized');
        }
        const { id } = req.params;
        const expense = (0, expense_1.findExpenseByIdAndSchoolId)(+id, payload.schoolId);
        res.json(expense);
    }
    catch (error) {
        next(error);
    }
});
router.get('/expenses', auth_1.isAuth, async (req, res, next) => {
    try {
        const payload = req.payload;
        if (payload.role !== client_1.RoleEnumType.admin) {
            res.status(401);
            throw new Error('Unauthorized');
        }
        const expenses = await (0, expense_1.findExpensesBySchoolId)(payload.schoolId);
        res.json(expenses);
    }
    catch (error) {
        next(error);
    }
});
router.post('/expense', auth_1.isAuth, async (req, res, next) => {
    try {
        const payload = req.payload;
        if (payload.role !== client_1.RoleEnumType.admin) {
            res.status(401);
            throw new Error('Unauthorized');
        }
        const expense = await (0, expense_1.createExpense)(req.body);
        res.json(expense);
    }
    catch (error) {
        next(error);
    }
});
router.put('/expense/:id', auth_1.isAuth, async (req, res, next) => {
    try {
        const payload = req.payload;
        if (payload.role !== client_1.RoleEnumType.admin && payload.email !== configs_1.SUPER_ADMIN_EMAIL) {
            res.status(401);
            throw new Error('Unauthorized');
        }
        const { id } = req.params;
        const expense = await (0, expense_1.updateExpenseById)(+id, req.body);
        res.json(expense);
    }
    catch (error) {
        next(error);
    }
});
router.delete('/expense/:id', auth_1.isAuth, async (req, res, next) => {
    try {
        const payload = req.payload;
        if (payload.role !== client_1.RoleEnumType.admin && payload.email !== configs_1.SUPER_ADMIN_EMAIL) {
            res.status(401);
            throw new Error('Unauthorized');
        }
        const { id } = req.params;
        const expense = await (0, expense_1.deleteExpenseById)(+id);
        res.json(expense);
    }
    catch (error) {
        next(error);
    }
});
exports.default = router;
