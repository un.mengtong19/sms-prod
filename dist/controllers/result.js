"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const auth_1 = require("../middlewares/auth");
const result_1 = require("../services/result");
const client_1 = require("@prisma/client");
const student_1 = require("../services/student");
const teacher_1 = require("../services/teacher");
const router = (0, express_1.Router)();
/**
 * GET /result/:id
 *
 * @summary get a result
 * @param {number} id the id fo the result
 * @returns {object} 200 - success response
 * @returns {object} 401 - Unauthorized response
 * @example response - 200 - success response resultple
 *   {
 *     "_id": "Bury the light",
 *     "name": "lorem ipsum",
 *   }
 */
router.get('/result/:id', auth_1.isAuth, async (req, res, next) => {
    try {
        const id = req.params.id;
        const result = await (0, result_1.findResultById)(+id);
        res.json(result);
    }
    catch (error) {
        next(error);
    }
});
/**
 * @summary Get All Results
 */
router.get('/results', auth_1.isAuth, async (req, res, next) => {
    try {
        const payload = req.payload;
        switch (payload.role) {
            case client_1.RoleEnumType.student:
                const student = await (0, student_1.findStudentByUserId)(payload.userId);
                const resultsStudent = await (0, result_1.findResultByStudentId)(student.id);
                res.json(resultsStudent);
                break;
            case client_1.RoleEnumType.admin:
                const results = await (0, result_1.findAllResults)(payload.schoolId);
                res.json(results);
                break;
            case client_1.RoleEnumType.teacher:
                const teacher = await (0, teacher_1.findTeacherByUserId)(payload.userId);
                const resultsTeacher = await (0, result_1.findResultsByTeacher)(teacher.id);
                res.json(resultsTeacher);
            default:
                res.status(401);
                throw new Error('🚫User is Un-Authorized 🚫');
        }
    }
    catch (error) {
        next(error);
    }
});
/**
 * @summary Create Result
 */
router.post('/result', auth_1.isAuth, async (req, res, next) => {
    try {
        const payload = req.payload;
        if (payload.role === client_1.RoleEnumType.admin || payload.role === client_1.RoleEnumType.teacher) {
            const result = await (0, result_1.createResult)(req.body);
            res.json(result);
        }
        else {
            res.status(401);
            throw new Error('🚫User is Un-Authorized 🚫');
        }
    }
    catch (error) {
        next(error);
    }
});
/**
 * @summary Update Result
 */
router.put('/result/:id', auth_1.isAuth, async (req, res, next) => {
    try {
        const payload = req.payload;
        const id = req.params.id;
        const result = await (0, result_1.findResultById)(+id);
        if (result) {
            if (payload.role === client_1.RoleEnumType.admin || payload.role === client_1.RoleEnumType.teacher) {
                const updatedResult = await (0, result_1.updateResult)(+id, req.body);
                res.json({ updatedResult });
            }
            else {
                res.status(401);
                throw new Error('🚫User is Un-Authorized 🚫');
            }
        }
        else {
            res.status(404);
            throw new Error('🚫Result not found 🚫');
        }
    }
    catch (error) {
        next(error);
    }
});
/**
 * @summary Delete Result
 * @param {number} id the id fo the result
 * @returns {object} 200 - success response
 * @returns {object} 401 - Unauthorized response
 * @example response - 200 - success response resultple
 *
 **/
router.delete('/result/:id', auth_1.isAuth, async (req, res, next) => {
    try {
        const payload = req.payload;
        const id = req.params.id;
        const result = await (0, result_1.findResultById)(+id);
        if (result) {
            if (payload.role === client_1.RoleEnumType.admin || payload.role === client_1.RoleEnumType.teacher) {
                const deletedResult = await (0, result_1.deleteResult)(+id);
                res.json(deletedResult);
            }
            else {
                res.status(401);
                throw new Error('🚫User is Un-Authorized 🚫');
            }
        }
        else {
            res.status(404);
            throw new Error('🚫Result not found 🚫');
        }
    }
    catch (error) {
        next(error);
    }
});
exports.default = router;
