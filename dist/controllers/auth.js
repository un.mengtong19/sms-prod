"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const uuid_1 = require("uuid");
const auth_1 = require("../services/auth");
const router = express_1.default.Router();
const user_1 = require("../services/user");
const jwt_1 = require("../utils/jwt");
const school_1 = require("../services/school");
const bcrypt_1 = __importDefault(require("bcrypt"));
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
/**
 * POST /register
 * @summary  register a school
 * @param {string} email the email of the new school
 * @param {string} password the password of the new school
 * @param {string} name the name of the new school
 * @param {string} address the address of the new school
 * @param {string} phone the phone of the new school
 * @param {string} website the website of the new school
 * @param {string} logo the logo of the new school
 * @param {string} description the description of the new school
 * @returns {object} 200 - success response
 * @returns {object} 400 - Bad request response
 * @example response - 200 - success response example
 *  {
 *   "accessToken":"accessToken",
 *  "refreshToken":"refreshToken"
 */
router.post('/register', async (req, res, next) => {
    try {
        // Create School
        const { email, password, name, address, phone, website } = req.body;
        if (!email || !password || !name) {
            res.status(400);
            throw new Error('You must provide an email, password, and name.');
        }
        // Check Exist School name or user
        const existingSchool = await (0, school_1.findSchoolByName)(name);
        const existingUser = await (0, user_1.findUserByEmail)(email);
        if (existingSchool || existingUser) {
            res.status(400);
            throw new Error('School name or user already in use.');
        }
        // Validate Input
        const schoolData = {
            phone,
            email,
            address,
            website,
            logo: '',
            description: '',
            name,
            createdAt: new Date(),
            deletedAt: null,
            updatedAt: new Date(),
        };
        // Create School
        const school = await (0, school_1.createSchool)(schoolData);
        const userData = {
            email,
            password,
            schoolId: school.id,
            role: 'admin',
        };
        const user = await (0, user_1.createUserByEmailAndPassword)(userData);
        const jti = (0, uuid_1.v4)();
        const { accessToken, refreshToken } = (0, jwt_1.generateTokens)(user, jti);
        await (0, auth_1.addRefreshTokenToWhitelist)({ jti, refreshToken, userId: user.id });
        res.json({
            accessToken,
            refreshToken,
        });
    }
    catch (err) {
        next(err);
    }
});
router.post('/login', async (req, res, next) => {
    try {
        const { email, password } = req.body;
        if (!email || !password) {
            res.status(400);
            throw new Error('You must provide an email and a password.');
        }
        const existingUser = await (0, user_1.findUserByEmail)(email);
        if (!existingUser) {
            res.status(403);
            throw new Error('Invalid login credentials.');
        }
        const validPassword = await bcrypt_1.default.compare(password, existingUser.password);
        if (!validPassword) {
            res.status(403);
            throw new Error('Invalid login credentials.');
        }
        const jti = (0, uuid_1.v4)();
        const { accessToken, refreshToken } = (0, jwt_1.generateTokens)(existingUser, jti);
        await (0, auth_1.addRefreshTokenToWhitelist)({ jti, refreshToken, userId: existingUser.id });
        res.json({
            accessToken,
            refreshToken,
        });
    }
    catch (err) {
        next(err);
    }
});
router.post('/refreshToken', async (req, res, next) => {
    try {
        const { refreshToken } = req.body;
        if (!refreshToken) {
            res.status(400);
            throw new Error('Missing refresh token.');
        }
        const payload = jsonwebtoken_1.default.verify(refreshToken, process.env.JWT_REFRESH_SECRET);
        const savedRefreshToken = await (0, auth_1.findRefreshTokenById)(payload.jti);
        if (!savedRefreshToken || savedRefreshToken.revoked === true) {
            res.status(401);
            throw new Error('Unauthorized');
        }
        const hashedToken = (0, jwt_1.hashToken)(refreshToken);
        if (hashedToken !== savedRefreshToken.hashedToken) {
            res.status(401);
            throw new Error('Unauthorized');
        }
        const user = await (0, user_1.findUserById)(payload.userId);
        if (!user) {
            res.status(401);
            throw new Error('Unauthorized');
        }
        await (0, auth_1.deleteRefreshToken)(savedRefreshToken.id);
        const jti = (0, uuid_1.v4)();
        const { accessToken, refreshToken: newRefreshToken } = (0, jwt_1.generateTokens)(user, jti);
        await (0, auth_1.addRefreshTokenToWhitelist)({ jti, refreshToken: newRefreshToken, userId: user.id });
        res.json({
            accessToken,
            refreshToken: newRefreshToken,
        });
    }
    catch (err) {
        next(err);
    }
});
// This endpoint is only for demo purpose.
// Move this logic where you need to revoke the tokens( for ex, on password reset)
router.post('/revokeRefreshTokens', async (req, res, next) => {
    try {
        const { userId } = req.body;
        await (0, auth_1.revokeTokens)(userId);
        res.json({ message: `Tokens revoked for user with id #${userId}` });
    }
    catch (err) {
        next(err);
    }
});
exports.default = router;
