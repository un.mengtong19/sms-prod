"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const client_1 = require("@prisma/client");
const express_1 = require("express");
const auth_1 = require("../middlewares/auth");
const db_1 = require("../prisma/db");
const school_1 = require("../services/school");
const teacher_1 = require("../services/teacher");
const student_1 = require("../services/student");
const router = (0, express_1.Router)();
/**
 * POST /tag
 *
 * @summary create a tag
 * @param {string} name the name fo the new tag
 * @returns {object} 200 - success response
 * @returns {object} 400 - Bad request response
 * @example response - 200 - success response example
 *   {
 *     "id": 1,
 *     "name": "lorem ipsum",
 *   }
 */
router.get('/users', auth_1.isAuth, async (req, res, next) => {
    try {
        const payload = req.payload;
        console.log(payload);
        if (payload.role !== client_1.RoleEnumType.admin) {
            res.status(401);
            throw new Error('Unauthorized');
        }
        const users = await db_1.prisma.user.findMany();
        res.json(users);
    }
    catch (error) {
        next(error);
    }
});
router.get('/profile', auth_1.isAuth, async (req, res, next) => {
    try {
        const payload = req.payload;
        switch (payload.role) {
            case client_1.RoleEnumType.admin:
                const schoolPf = await (0, school_1.findSchoolById)(payload.schoolId);
                const schoolData = { ...schoolPf, role: payload.role };
                res.json(schoolData);
                break;
            case client_1.RoleEnumType.teacher:
                const teacherPf = await (0, teacher_1.findTeacherByUserId)(payload.userId);
                const teacherData = { ...teacherPf, role: payload.role };
                res.json(teacherData);
                break;
            case client_1.RoleEnumType.student:
                const studentPf = await (0, student_1.findStudentByUserId)(payload.userId);
                const studentData = { ...studentPf, role: payload.role };
                res.json(studentData);
            default:
                res.status(400);
                throw new Error('Invalid role');
        }
    }
    catch (err) {
        next(err);
    }
});
exports.default = router;
