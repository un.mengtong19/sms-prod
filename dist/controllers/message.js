"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const auth_1 = require("../middlewares/auth");
const message_1 = require("../services/message");
const configs_1 = require("../configs");
const router = (0, express_1.Router)();
/**
 * GET /message/:id
 *
 * @summary get a message
 * @param {number} id the id fo the message
 * @returns {object} 200 - success response
 * @returns {object} 401 - Unauthorized response
 * @example response - 200 - success response messageple
 *   {
 *     "_id": "Bury the light",
 *     "name": "lorem ipsum",
 *   }
 */
router.get('/message/:id', auth_1.isAuth, async (req, res, next) => {
    try {
        const id = req.params.id;
        const payload = req.payload;
        if (!['teacher', 'student', 'admin'].includes(payload.role)) {
            res.status(401);
            throw new Error('🚫User is Un-Authorized 🚫');
        }
        const message = await (0, message_1.findMessageById)(+id);
        res.json(message);
    }
    catch (error) {
        next(error);
    }
});
/**
 * @summary Get all messages
 */
router.get('/messages', auth_1.isAuth, async (req, res, next) => {
    try {
        const payload = req.payload;
        if (!payload.role) {
            res.status(401);
            throw new Error('🚫User is Un-Authorized 🚫');
        }
        if (payload.email == configs_1.SUPER_ADMIN_EMAIL) {
            const messages = await (0, message_1.findAllMessages)();
            res.json({ messages });
            return;
        }
        const userMessages = await (0, message_1.findMessagesByUserId)(payload.userId);
        res.json(userMessages);
    }
    catch (error) {
        next(error);
    }
});
/**
 * @summary Create a message
 */
router.post('/message', auth_1.isAuth, async (req, res, next) => {
    try {
        const payload = req.payload;
        if (!payload.role) {
            res.status(401);
            throw new Error('🚫User is Un-Authorized 🚫');
        }
        const message = await (0, message_1.createMessage)(req.body);
        res.json(message);
    }
    catch (error) {
        next(error);
    }
});
/**
 * @summary Update a message
 */
router.put('/message/:id', auth_1.isAuth, async (req, res, next) => {
    try {
        const payload = req.payload;
        if (!payload.role) {
            res.status(401);
            throw new Error('🚫User is Un-Authorized 🚫');
        }
        const id = req.params.id;
        const currMsg = await (0, message_1.findMessageById)(+id);
        if (currMsg.userId !== payload.userId) {
            res.status(401);
            throw new Error('🚫User is Un-Authorized 🚫');
        }
        const message = await (0, message_1.updateMessage)(+id, req.body);
        res.json(message);
    }
    catch (error) {
        next(error);
    }
});
/**
 * @summary Delete a message
 */
router.delete('/message/:id', auth_1.isAuth, async (req, res, next) => {
    try {
        const payload = req.payload;
        if (!payload.role) {
            res.status(401);
            throw new Error('🚫User is Un-Authorized 🚫');
        }
        const id = req.params.id;
        const currMsg = await (0, message_1.findMessageById)(+id);
        if (currMsg.userId !== payload.userId) {
            res.status(401);
            throw new Error('🚫User is Un-Authorized 🚫');
        }
        const message = await (0, message_1.deleteMessage)(+id);
        res.json(message);
    }
    catch (error) {
        next(error);
    }
});
exports.default = router;
