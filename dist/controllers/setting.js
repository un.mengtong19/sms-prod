"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const auth_1 = require("../middlewares/auth");
const configs_1 = require("../configs");
const setting_1 = require("../services/setting");
const router = (0, express_1.Router)();
router.get('/setting/:id', auth_1.isAuth, async (req, res, next) => {
    try {
        const payload = req.payload;
        const id = req.params.id;
        if (payload.email !== configs_1.SUPER_ADMIN_EMAIL) {
            res.status(401);
            throw new Error('Unauthorized');
        }
        const setting = await (0, setting_1.findSettingById)(+id);
        if (setting) {
            res.json(setting);
        }
        else {
            res.status(404);
            throw new Error('Setting not found');
        }
    }
    catch (error) {
        next(error);
    }
});
router.get('/settings', auth_1.isAuth, async (req, res, next) => {
    try {
        const payload = req.payload;
        if (payload.email !== configs_1.SUPER_ADMIN_EMAIL) {
            res.status(401);
            throw new Error('Unauthorized');
        }
        const settings = await (0, setting_1.findAllSettings)();
        if (settings) {
            res.json({ settings });
        }
        else {
            res.status(404);
            throw new Error('Settings not found');
        }
    }
    catch (error) {
        next(error);
    }
});
router.post('/setting', auth_1.isAuth, async (req, res, next) => {
    try {
        const payload = req.payload;
        if (payload.email !== configs_1.SUPER_ADMIN_EMAIL) {
            res.status(401).json({ message: 'Unauthorized' });
        }
        const { name, value } = req.body;
        if (!name || !value) {
            res.status(400);
            throw new Error("Bad request ...");
        }
        const settingData = {
            name,
            value,
            schoolId: payload.schoolId
        };
        const newSetting = await (0, setting_1.createSetting)(settingData);
        res.json(newSetting);
    }
    catch (error) {
        next(error);
    }
});
router.put('/setting/:id', auth_1.isAuth, async (req, res, next) => {
    try {
        const payload = req.payload;
        if (payload.email !== configs_1.SUPER_ADMIN_EMAIL) {
            res.status(401).json({ message: 'Unauthorized' });
        }
        const id = req.params.id;
        const { name, value } = req.body;
        const settingData = {
            name,
            value
        };
        const newSetting = await (0, setting_1.updateSetting)(+id, settingData);
        res.json(newSetting);
    }
    catch (error) {
        next(error);
    }
});
router.delete('/setting/:id', auth_1.isAuth, async (req, res, next) => {
    try {
        const payload = req.payload;
        if (payload.email !== configs_1.SUPER_ADMIN_EMAIL) {
            res.status(401).json({ message: 'Unauthorized' });
        }
        const id = req.params.id;
        const setting = await (0, setting_1.deleteSetting)(+id);
        res.json(setting);
    }
    catch (error) {
        next(error);
    }
});
exports.default = router;
