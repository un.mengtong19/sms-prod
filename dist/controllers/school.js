"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const auth_1 = require("../middlewares/auth");
const configs_1 = require("../configs");
const school_1 = require("../services/school");
const router = (0, express_1.Router)();
/**
 * GET /school/:id
 * @summary get a school
 * @Auth Super Admin Only
 * @param {string} id the id fo the school
 * @returns {object} 200 - success response
 * @returns {object} 401 - Unauthorized response
 * @example response - 200 - success response example
 *  {
 *    "id": 1,
 *    "name": "lorem ipsum",
 *    "email": "school@email.com",
 *    "address": "lorem ipsum",
 *    "phone": "lorem ipsum",
 *    "website": "www.loremipsum.com",
 *    "logo": "https://loremipsum.com/logo.png",
 *    "description": "lorem ipsum",
 *    "createdAt": "2021-05-05T12:00:00.000Z",
 *    "updatedAt": "2021-05-05T12:00:00.000Z"
 * }
 *
 **/
router.get('/school/:id', auth_1.isAuth, async (req, res, next) => {
    try {
        const payload = req.payload;
        if (payload.email !== configs_1.SUPER_ADMIN_EMAIL) {
            res.sendStatus(401);
            throw new Error('Unauthorized');
        }
        const id = req.params.id;
        const school = await (0, school_1.findSchoolById)(+id);
        res.json(school);
    }
    catch (error) {
        next(error);
    }
});
/**
 * GET /school
 * @summary get all schools
 * @Auth Super Admin Only
 * @returns {object} 200 - success response
 * @returns {object} 401 - Unauthorized response
 * @example response - 200 - success response example
 * {
 *  "schools": [
 *   {
 *   "id": 1,
 *   "name": "lorem ipsum",
 *   "email": "
 *   "address": "lorem ipsum",
 *   "phone": "lorem ipsum",
 *   "website": "www.loremipsum.com",
 *   "logo": "https://loremipsum.com/logo.png",
 *   "description": "lorem ipsum",
 *   "createdAt": "2021-05-05T12:00:00.000Z",
 *   "updatedAt": "2021-05-05T12:00:00.000Z"
 **/
router.get('/schools', auth_1.isAuth, async (req, res, next) => {
    try {
        const payload = req.payload;
        if (payload.email !== configs_1.SUPER_ADMIN_EMAIL) {
            res.sendStatus(401);
            throw new Error('Unauthorized');
        }
        const schools = await (0, school_1.findSchools)();
        res.json(schools);
    }
    catch (error) {
        next(error);
    }
});
/**
 * POST /school
 * @summary create a school
 * @Auth Super Admin Only
 * @returns {object} 200 - success response
 * @returns {object} 401 - Unauthorized response
 * @example response - 200 - success response example
 * {
 *  "id": 1,
 *  "name": "lorem ipsum",
 *  "email": "
 *  "address": "lorem ipsum",
 *  "phone": "lorem ipsum",
 *  "website": "www.loremipsum.com",
 *  "logo": "https://loremipsum.com/logo.png",
 *  "description": "lorem ipsum",
 *  "createdAt": "2021-05-05T12:00:00.000Z",
 *  "updatedAt": "2021-05-05T12:00:00.000Z"
 * }
 **/
router.post('/school', auth_1.isAuth, async (req, res, next) => {
    try {
        const payload = req.payload;
        if (payload.email !== configs_1.SUPER_ADMIN_EMAIL) {
            res.sendStatus(401);
            throw new Error('Unauthorized');
        }
        const { name, email, address, phone, website, logo, description } = req.body;
        const schoolData = {
            name,
            email,
            address,
            phone,
            website,
            logo,
            description,
            createdAt: new Date(),
            updatedAt: new Date(),
        };
        const school = await (0, school_1.createSchool)(schoolData);
        res.json(school);
    }
    catch (error) {
        next(error);
    }
});
/**
 * PUT /school/:id
 * @summary update a school
 * @Auth Super Admin Only
 * @param {string} id the id fo the school
 * @returns {object} 200 - success response
 * @returns {object} 401 - Unauthorized response
 * @example response - 200 - success response example
 * {
 *   "id": 1,
 *   "name": "lorem ipsum",
 *   "email": "
 *   "address": "lorem ipsum",
 *   "phone": "lorem ipsum",
 *   "website": "www.loremipsum.com",
 *   "logo": "https://loremipsum.com/logo.png",
 *   "description": "lorem ipsum",
 *   "createdAt": "2021-05-05T12:00:00.000Z",
 *   "updatedAt": "2021-05-05T12:00:00.000Z"
 * }
 **/
router.put('/school/:id', auth_1.isAuth, async (req, res, next) => {
    try {
        const payload = req.payload;
        if (payload.email !== configs_1.SUPER_ADMIN_EMAIL) {
            res.sendStatus(401);
            throw new Error('Unauthorized');
        }
        const id = req.params.id;
        const { name, email, address, phone, website, logo, description } = req.body;
        const schoolData = {
            name,
            email,
            address,
            phone,
            website,
            logo,
            description,
        };
        const school = await (0, school_1.updateSchool)(+id, schoolData);
        res.json(school);
    }
    catch (error) {
        next(error);
    }
});
/**
 * DELETE /school/:id
 * @summary delete a school
 * @Auth Super Admin Only
 * @param {string} id the id fo the school
 * @returns {object} 200 - success response
 * @returns {object} 401 - Unauthorized response
 * @example response - 200 - success response example
 * {
 *  "message": "School deleted"
 * }
 * @example response - 401 - Unauthorized response example
 * {
 * "message": "Unauthorized"
 * }
 **/
router.delete('/school/:id', auth_1.isAuth, async (req, res, next) => {
    try {
        const payload = req.payload;
        if (payload.email !== configs_1.SUPER_ADMIN_EMAIL) {
            res.sendStatus(401);
            throw new Error('Unauthorized');
        }
        const id = req.params.id;
        const delSchool = await (0, school_1.deleteSchool)(+id);
        res.json(delSchool);
    }
    catch (error) {
        next(error);
    }
});
exports.default = router;
