"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const client_1 = require("@prisma/client");
const express_1 = require("express");
const auth_1 = require("../middlewares/auth");
const exam_1 = require("../services/exam");
const student_1 = require("../services/student");
const router = (0, express_1.Router)();
/**
 *
 * @summary get all exam
 * @route {GET} /exams
 * @auth required
 * @param {number} id the id of the exam
 * @returns {object} 200 - success response
 * @returns {object} 400 - bad request fail respone
 * @returns {object} 401 - Unauthorized response
 * @example response - 200 - success response subject
 *
 *   [{
 *     "id": 1,
 *     "name": "lorem ipsum",
 *     "date": "1970-01-01T00:00:00.000Z",
 *     "schoolId": 1,
 *     "subjectId": 3,
 *     "subject": {
 *       "id": 2,
 *        "name": "Subject 2"
 *     }
 *   }]
 */
// switch (payload.role) {
//   case RoleEnumType.student:
//     res.json('heklsa');
//     break;
//   default:
//     res.status(401);
//     throw new Error('🚫User is Un-Authorized 🚫');
// }
router.get('/exams', auth_1.isAuth, async (req, res, next) => {
    try {
        const payload = req.payload;
        switch (payload.role) {
            case client_1.RoleEnumType.admin:
                const exams = await (0, exam_1.findAllExamBySchoolId)(payload.schoolId);
                res.json(exams);
                break;
            case client_1.RoleEnumType.student:
                const student = await (0, student_1.findStudentByUserId)(+payload.userId);
                const data = await (0, exam_1.findAllExamOfStudent)(student.id, +payload.schoolId);
                res.json(data);
                break;
            default:
                res.status(401);
                throw new Error('🚫User is Un-Authorized 🚫');
        }
    }
    catch (error) {
        next(error);
    }
});
/**
 *
 * @summary get a unique exam by Id
 * @route {GET} /exam/:id
 * @auth required
 * @param {number} id the id of the exam
 * @returns {object} 200 - success response
 * @returns {object} 400 - bad request fail respone
 * @returns {object} 401 - Unauthorized response
 * @example response - 200 - success response subject
 *
 *   {
 *     "id": 1,
 *     "name": "lorem ipsum",
 *     "date": "1970-01-01T00:00:00.000Z",
 *     "schoolId": 1,
 *     "subjectId": 3
 *   }
 */
router.get('/exam/:id', auth_1.isAuth, async (req, res, next) => {
    try {
        // checking role:
        const payload = req.payload;
        if (payload.role !== client_1.RoleEnumType.admin) {
            res.status(401);
            throw new Error('🚫User is Un-Authorized 🚫');
        }
        const id = req.params.id;
        const exam = await (0, exam_1.findExamByIdAndSchoolId)(+id, +payload.schoolId);
        res.json(exam);
    }
    catch (error) {
        next(error);
    }
});
/**
 *
 * @summary delete a unique exam by Id
 * @route {DELETE} /exam/:id
 * @auth required
 * @param {number} id the id of the exam
 * @returns {object} 200 - success response
 * @returns {object} 400 - bad request fail respone
 * @returns {object} 401 - Unauthorized response
 * @example response - 200 - success response an object deleted ...
 *   {
 *      "message": "⛔️1 exam deleted ..."
 *   }
 */
router.delete('/exam/:id', auth_1.isAuth, async (req, res, next) => {
    try {
        // checking role:
        const payload = req.payload;
        if (payload.role !== client_1.RoleEnumType.admin) {
            res.status(401);
            throw new Error('🚫User is Un-Authorized 🚫');
        }
        const id = req.params.id;
        const examDeleted = await (0, exam_1.deleteExamByIdAndSchoolId)(+id, +payload.schoolId);
        res.json(examDeleted);
    }
    catch (error) {
        next(error);
    }
});
/**
 *
 * @summary create a new exam
 * @route {POST} /subject
 * @auth required
 * @bodyparam {string} name the name of the exam
 * @bodyparam {string} date the code of the date
 * @bodyparam {number} subjectId the subjectId of th exam (it must be match with id of subject)
 * @bodyparam {number} schoolId the schoolId of the exam
 * @returns {object} 200 - success response
 * @returns {object} 400 - bad request fail respone
 * @returns {object} 401 - Unauthorized response
 * @example response - 200 - success response an object message subject created
 *   {
 *      "message": "🆕1 exam created ..."
 *   }
 */
router.post('/exam', auth_1.isAuth, async (req, res, next) => {
    try {
        // checking role:
        const payload = req.payload;
        if (payload.role !== client_1.RoleEnumType.admin) {
            res.status(401);
            throw new Error('🚫User is Un-Authorized 🚫');
        }
        // check input:
        const { name, date, subjectId } = req.body;
        if (!name || !date || !subjectId) {
            res.status(400);
            throw new Error('You must input name, date and subjectId');
        }
        // input validate
        const examData = {
            name,
            date,
            schoolId: payload.schoolId,
            subjectId,
        };
        // create exam
        const exam = await (0, exam_1.createExam)(examData);
        res.json(exam);
    }
    catch (error) {
        next(error);
    }
});
/**
 *
 * @summary update exam data by Id (not able to change schoolId for each exam)
 * @route {PUT} /exam/:id
 * @auth required
 * @bodyparam {string} name the name of the exam
 * @bodyparam {string} date the date of the date
 * @bodyparam {number} subjectId the subjectId of th exam(it must be match with id of subject)
 * @returns {object} 200 - success response
 * @returns {object} 400 - bad request fail respone
 * @returns {object} 401 - Unauthorized response
 * @example response - 200 - success response an object message subject updated
 *   {
 *      "message": "💯1 exam updated..."
 *   }
 */
router.put('/exam/:id', auth_1.isAuth, async (req, res, next) => {
    try {
        const payload = req.payload;
        if (payload.role !== client_1.RoleEnumType.admin) {
            res.status(401);
            throw new Error('🚫User is Un-Authorized 🚫');
        }
        const id = req.params.id;
        const { name, date, subjectId } = req.body;
        if (!name || !date || !subjectId) {
            res.status(400);
            throw new Error("🚫name , date and subjectId can't invalid ... ");
        }
        // input validate
        const examData = {
            name,
            date,
            subjectId,
        };
        // update data
        const newExam = await (0, exam_1.updateExamByIdAndSchoolId)(+id, +payload.schoolId, examData);
        res.json(newExam);
    }
    catch (error) {
        next(error);
    }
});
exports.default = router;
