"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MAX_LIMIT = exports.SUPER_ADMIN_EMAIL = void 0;
exports.SUPER_ADMIN_EMAIL = process.env.SUPER_ADMIN_EMAIL || 'super_admin';
exports.MAX_LIMIT = 100;
