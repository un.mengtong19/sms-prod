"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteClass = exports.updateClass = exports.createClass = exports.findAllClassBySchoolId = exports.findClassById = void 0;
const db_1 = require("../prisma/db");
const findClassById = async (id) => {
    return await db_1.prisma.class.findUnique({
        where: {
            id,
        },
    });
};
exports.findClassById = findClassById;
const findAllClassBySchoolId = async (schoolId) => {
    return await db_1.prisma.class.findMany({
        where: {
            schoolId,
        },
        orderBy: {
            name: 'asc',
        },
    });
};
exports.findAllClassBySchoolId = findAllClassBySchoolId;
const createClass = async (data) => {
    return await db_1.prisma.class.create({
        data,
    });
};
exports.createClass = createClass;
const updateClass = async (id, data) => {
    return await db_1.prisma.class.update({
        where: {
            id,
        },
        data,
    });
};
exports.updateClass = updateClass;
const deleteClass = async (id) => {
    return await db_1.prisma.class.delete({
        where: {
            id,
        },
    });
};
exports.deleteClass = deleteClass;
