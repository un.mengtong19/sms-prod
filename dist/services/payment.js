"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.findPaymentByIdAndSchoolId = exports.findPaymentsBySchoolId = exports.deletePaymentById = exports.updatePaymentById = exports.createPayment = exports.findAllPayments = exports.findPaymentById = void 0;
const db_1 = require("../prisma/db");
const findPaymentById = async (id) => {
    return await db_1.prisma.payment.findUnique({
        where: {
            id,
        },
    });
};
exports.findPaymentById = findPaymentById;
const findAllPayments = async () => {
    return await db_1.prisma.payment.findMany();
};
exports.findAllPayments = findAllPayments;
const createPayment = async (payment) => {
    return await db_1.prisma.payment.create({
        data: payment,
    });
};
exports.createPayment = createPayment;
const updatePaymentById = async (id, payment) => {
    return await db_1.prisma.payment.update({
        where: {
            id,
        },
        data: payment,
    });
};
exports.updatePaymentById = updatePaymentById;
const deletePaymentById = async (id) => {
    return await db_1.prisma.payment.delete({
        where: {
            id,
        },
    });
};
exports.deletePaymentById = deletePaymentById;
const findPaymentsBySchoolId = async (schoolId) => {
    return await db_1.prisma.payment.findMany({
        where: {
            schoolId,
        },
    });
};
exports.findPaymentsBySchoolId = findPaymentsBySchoolId;
const findPaymentByIdAndSchoolId = async (id, schoolId) => {
    return await db_1.prisma.payment.findFirst({
        where: {
            AND: [{ id }, { schoolId }],
        },
        include: {
            Student: true,
            Teacher: true,
        },
    });
};
exports.findPaymentByIdAndSchoolId = findPaymentByIdAndSchoolId;
