"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteTeacherByIdAndSchoolId = exports.updateTeacherByIdAndSchoolId = exports.findTeacherByIdAndSchoolId = exports.findAllTeacherBySchoolId = exports.createTeacherBySchoolId = exports.findTeacherByUserId = exports.deleteTeacherData = exports.updateTeacherData = exports.findAllTeachers = exports.createTeacherData = exports.findTeacherById = void 0;
const client_1 = require("@prisma/client");
const db_1 = require("../prisma/db");
const user_1 = require("./user");
// Find
const findTeacherByUserId = async (userId) => {
    return await db_1.prisma.teacher.findFirst({
        where: {
            userId,
        },
    });
};
exports.findTeacherByUserId = findTeacherByUserId;
const findTeacherById = async (id) => {
    return await db_1.prisma.teacher.findUnique({
        where: {
            id,
        },
    });
};
exports.findTeacherById = findTeacherById;
const findTeacherByIdAndSchoolId = async (id, schoolId) => {
    const existingTeacher = await findTeacherById(+id);
    if (!existingTeacher) {
        throw new Error(' teacher not found ...');
    }
    if (existingTeacher.schoolId !== schoolId) {
        throw new Error('user is Un-authorized ...');
    }
    return existingTeacher;
};
exports.findTeacherByIdAndSchoolId = findTeacherByIdAndSchoolId;
const findAllTeachers = async () => {
    return await db_1.prisma.teacher.findMany();
};
exports.findAllTeachers = findAllTeachers;
const findAllTeacherBySchoolId = async (schoolId) => {
    const arrOfTeachers = await db_1.prisma.teacher.findMany({
        where: {
            schoolId,
        },
    });
    if (!arrOfTeachers) {
        throw new Error('Student not found ...');
    }
    return arrOfTeachers;
};
exports.findAllTeacherBySchoolId = findAllTeacherBySchoolId;
// update
const updateTeacherData = async (id, teacher) => {
    return await db_1.prisma.teacher.update({
        where: {
            id,
        },
        data: teacher,
    });
};
exports.updateTeacherData = updateTeacherData;
const updateTeacherByIdAndSchoolId = async (id, teacherDataInput, schoolId) => {
    await findTeacherByIdAndSchoolId(+id, +schoolId); // call function to check if teacher not found, it will throw error from : findTeacherByIdAndSchoolId()
    const email = teacherDataInput.email;
    const existingTeacher = await findTeacherById(+id);
    const existingEmail = await (0, user_1.findUserByEmail)(email);
    if (existingEmail && existingTeacher.email !== teacherDataInput.email) {
        throw new Error('Email already in used ...');
    }
    // update
    const newUserData = {
        email: teacherDataInput.email,
        password: teacherDataInput.password,
    };
    const userId = existingTeacher.userId;
    const newUser = await (0, user_1.updateUserById)(+userId, newUserData);
    // update
    const newStdData = {
        firstname: teacherDataInput.firstname,
        lastname: teacherDataInput.lastname,
        gender: teacherDataInput.gender,
        email: newUser.email,
        phone: teacherDataInput.phone,
        address: teacherDataInput.address,
    };
    return await updateTeacherData(+id, newStdData);
};
exports.updateTeacherByIdAndSchoolId = updateTeacherByIdAndSchoolId;
// Create
const createTeacherData = async (teacher) => {
    return await db_1.prisma.teacher.create({
        data: teacher,
    });
};
exports.createTeacherData = createTeacherData;
const createTeacherBySchoolId = async (schoolId, teacherDataInput) => {
    const existingEmail = await (0, user_1.findUserByEmail)(teacherDataInput.email);
    if (existingEmail) {
        throw new Error('Email is already in used.');
    }
    // creaete user
    const userData = {
        email: teacherDataInput.email,
        password: teacherDataInput.password,
        role: client_1.RoleEnumType.teacher,
        schoolId: schoolId,
    };
    const user = await (0, user_1.createUserByEmailAndPassword)(userData);
    // create student
    const teacherData = {
        firstname: teacherDataInput.firstname,
        lastname: teacherDataInput.lastname,
        gender: teacherDataInput.gender,
        email: user.email,
        phone: teacherDataInput.phone,
        address: teacherDataInput.address,
        schoolId: user.schoolId,
        userId: user.id,
    };
    return await createTeacherData(teacherData);
};
exports.createTeacherBySchoolId = createTeacherBySchoolId;
// Delete
const deleteTeacherData = async (id) => {
    return await db_1.prisma.teacher.delete({
        where: {
            id,
        },
    });
};
exports.deleteTeacherData = deleteTeacherData;
const deleteTeacherByIdAndSchoolId = async (id, schoolId) => {
    const teacher = await findTeacherByIdAndSchoolId(+id, schoolId);
    const userId = teacher.userId;
    // delete user --> Teacher also delete
    await (0, user_1.deleteUserById)(+userId);
    return teacher;
};
exports.deleteTeacherByIdAndSchoolId = deleteTeacherByIdAndSchoolId;
