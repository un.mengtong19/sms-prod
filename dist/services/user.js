"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.updateUserById = exports.deleteUserById = exports.createUserByEmailAndPassword = exports.findUserById = exports.findUserByEmail = void 0;
const db_1 = require("../prisma/db");
const bcrypt_1 = __importDefault(require("bcrypt"));
const findUserByEmail = async (email) => {
    return await db_1.prisma.user.findUnique({
        where: {
            email,
        },
    });
};
exports.findUserByEmail = findUserByEmail;
const createUserByEmailAndPassword = async (user) => {
    user.password = bcrypt_1.default.hashSync(user.password, 12);
    return await db_1.prisma.user.create({
        data: user,
    });
};
exports.createUserByEmailAndPassword = createUserByEmailAndPassword;
const findUserById = async (id) => {
    return await db_1.prisma.user.findUnique({
        where: {
            id,
        },
    });
};
exports.findUserById = findUserById;
const deleteUserById = async (id) => {
    return await db_1.prisma.user.delete({
        where: {
            id,
        },
    });
};
exports.deleteUserById = deleteUserById;
const updateUserById = async (id, user) => {
    user.password = bcrypt_1.default.hashSync(user.password, 12);
    return await db_1.prisma.user.update({
        where: {
            id,
        },
        data: user,
    });
};
exports.updateUserById = updateUserById;
