"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.findStudentsOfOneTeacher = exports.deleteStudentByIdAndSchoolId = exports.findStudentByIdAndSchoolId = exports.findStudentsBySchoolId = exports.findStudentByUserId = exports.deleteStudentById = exports.updateStudentByIdAndSchoolId = exports.findStudentByEmail = exports.findStudentById = exports.findAllStudents = exports.updateStudentData = exports.createStudent = void 0;
const db_1 = require("../prisma/db");
const client_1 = require("@prisma/client");
const user_1 = require("./user");
// create
const createStudent = async (schoolId, studentDataInput) => {
    const email = studentDataInput.email;
    const existingEmail = await (0, user_1.findUserByEmail)(email);
    if (existingEmail) {
        throw new Error('Email is already in used.');
    }
    // create user
    const userData = {
        email: studentDataInput.email,
        password: studentDataInput.password,
        role: client_1.RoleEnumType.student,
        schoolId: schoolId,
    };
    const user = await (0, user_1.createUserByEmailAndPassword)(userData);
    // create student
    const studentData = {
        firstname: studentDataInput.firstname,
        lastname: studentDataInput.lastname,
        gender: studentDataInput.gender,
        email: studentDataInput.email,
        phone: studentDataInput.phone,
        address: studentDataInput.address,
        schoolId: user.schoolId,
        userId: user.id,
    };
    return await db_1.prisma.student.create({
        data: studentData,
    });
};
exports.createStudent = createStudent;
// update
const updateStudentData = async (id, student) => {
    return await db_1.prisma.student.update({
        where: {
            id,
        },
        data: student,
    });
};
exports.updateStudentData = updateStudentData;
// find
const findAllStudents = async () => {
    return await db_1.prisma.student.findMany();
};
exports.findAllStudents = findAllStudents;
const findStudentsBySchoolId = async (schoolId) => {
    return await db_1.prisma.student.findMany({
        where: {
            schoolId,
        },
        orderBy: {
            firstname: 'asc',
        },
    });
};
exports.findStudentsBySchoolId = findStudentsBySchoolId;
const findStudentById = async (id) => {
    return await db_1.prisma.student.findUnique({
        where: {
            id,
        },
    });
};
exports.findStudentById = findStudentById;
const findStudentByIdAndSchoolId = async (id, schoolId) => {
    const existingStudent = await findStudentById(+id);
    if (!existingStudent) {
        throw new Error('student not found ...');
    }
    if (existingStudent.schoolId !== schoolId) {
        throw new Error('user is Un-authorized ...');
    }
    return existingStudent;
};
exports.findStudentByIdAndSchoolId = findStudentByIdAndSchoolId;
const findStudentByEmail = async (email) => {
    return await db_1.prisma.student.findUnique({
        where: {
            email,
        },
    });
};
exports.findStudentByEmail = findStudentByEmail;
const findStudentByUserId = async (userId) => {
    return await db_1.prisma.student.findFirst({
        where: {
            userId,
        },
    });
};
exports.findStudentByUserId = findStudentByUserId;
const findStudentsOfOneTeacher = async (teacherId, schoolId) => {
    const studentAttendances = await db_1.prisma.attendance.findMany({
        where: {
            AND: [{ teacherId }, { schoolId }],
        },
        distinct: ['studentId'],
        orderBy: {
            Student: { firstname: 'asc' },
        },
        select: {
            Student: true,
        },
    });
    const allStudentsOfTeacher = [];
    studentAttendances.forEach((data) => {
        allStudentsOfTeacher.push(data.Student);
    });
    return allStudentsOfTeacher;
};
exports.findStudentsOfOneTeacher = findStudentsOfOneTeacher;
const deleteStudentById = async (id) => {
    return await db_1.prisma.student.delete({
        where: {
            id,
        },
    });
};
exports.deleteStudentById = deleteStudentById;
// update user and student
const updateStudentByIdAndSchoolId = async (id, schoolId, studentDataInput) => {
    await findStudentByIdAndSchoolId(id, schoolId);
    const email = studentDataInput.email;
    const existingStudent = await findStudentById(+id);
    const existingEmail = await (0, user_1.findUserByEmail)(email);
    if (existingEmail && existingStudent.email !== email) {
        throw new Error('Email already in used ...');
    }
    // update User
    const newUserData = {
        email: studentDataInput.email,
        password: studentDataInput.password,
    };
    const userId = existingStudent.userId;
    const newUser = await (0, user_1.updateUserById)(+userId, newUserData);
    // update student
    const newStdData = {
        firstname: studentDataInput.firstname,
        lastname: studentDataInput.lastname,
        gender: studentDataInput.gender,
        email: newUser.email,
        phone: studentDataInput.phone,
        address: studentDataInput.address,
    };
    return await updateStudentData(+id, newStdData);
};
exports.updateStudentByIdAndSchoolId = updateStudentByIdAndSchoolId;
const deleteStudentByIdAndSchoolId = async (id, schoolId) => {
    const student = await findStudentByIdAndSchoolId(+id, schoolId);
    const userId = student.userId;
    // delete user --> student also delete
    await (0, user_1.deleteUserById)(+userId);
    return student;
};
exports.deleteStudentByIdAndSchoolId = deleteStudentByIdAndSchoolId;
