"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteSchool = exports.updateSchool = exports.findSchools = exports.findSchoolByName = exports.findSchoolById = exports.createSchool = void 0;
const db_1 = require("../prisma/db");
const createSchool = async (school) => {
    return db_1.prisma.school.create({
        data: school,
    });
};
exports.createSchool = createSchool;
const findSchoolById = async (id) => {
    return await db_1.prisma.school.findUnique({
        where: {
            id,
        },
    });
};
exports.findSchoolById = findSchoolById;
const findSchoolByName = async (name) => {
    return await db_1.prisma.school.findUnique({
        where: {
            name,
        },
    });
};
exports.findSchoolByName = findSchoolByName;
const findSchools = async () => {
    return await db_1.prisma.school.findMany();
};
exports.findSchools = findSchools;
const updateSchool = async (id, school) => {
    return await db_1.prisma.school.update({
        where: {
            id,
        },
        data: school,
    });
};
exports.updateSchool = updateSchool;
const deleteSchool = async (id) => {
    return await db_1.prisma.school.delete({
        where: {
            id,
        },
    });
};
exports.deleteSchool = deleteSchool;
