"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.updateExamData = exports.updateExamByIdAndSchoolId = exports.deleteExamData = exports.deleteExamByIdAndSchoolId = exports.createExam = exports.createExamData = exports.findAllExamOfStudent = exports.findExamByIdAndSchoolId = exports.findAllExamBySchoolId = exports.findAllExams = exports.findExamById = void 0;
const db_1 = require("../prisma/db");
const subject_1 = require("./subject");
// find
const findExamById = async (id) => {
    return await db_1.prisma.exam.findUnique({
        where: {
            id,
        },
    });
};
exports.findExamById = findExamById;
const findAllExams = async () => {
    return await db_1.prisma.exam.findMany();
};
exports.findAllExams = findAllExams;
const findAllExamBySchoolId = async (schoolId) => {
    const allExam = await db_1.prisma.exam.findMany({
        where: {
            schoolId,
        },
        orderBy: {
            subject: {
                id: 'asc',
            },
        },
        include: {
            subject: {
                select: {
                    id: true,
                    name: true,
                },
            },
        },
    });
    const examsOfSchool = [];
    allExam.forEach((data) => {
        examsOfSchool.push({
            id: data.id,
            name: data.name,
            date: data.date,
            subject: data.subject.name,
        });
    });
    // console.log(allExam);
    return examsOfSchool;
};
exports.findAllExamBySchoolId = findAllExamBySchoolId;
const findExamByIdAndSchoolId = async (id, schoolId) => {
    const curExam = await findExamById(+id);
    if (!curExam) {
        throw new Error('Bad request, exam not found ...');
    }
    if (curExam.schoolId !== schoolId) {
        throw new Error('Un-Authorized');
    }
    return curExam;
};
exports.findExamByIdAndSchoolId = findExamByIdAndSchoolId;
const findExamByIdSubjectIdAndSchoolId = async (id, subjectId, schoolId) => {
    const existingExam = await findExamById(+id);
    if (!existingExam) {
        throw new Error('Bad request, exam not found ...');
    }
    const existingSubject = await (0, subject_1.findSubjectById)(+subjectId);
    if (!existingSubject) {
        throw new Error('Bad request, subject not found');
    }
    if (existingSubject.schoolId !== schoolId || existingExam.schoolId !== schoolId) {
        throw new Error('User is Un-Authorized');
    }
    return existingExam;
};
const findAllExamOfStudent = async (studentId, schoolId) => {
    const allResults = await db_1.prisma.result.findMany({
        where: {
            AND: [{ schoolId }, { studentId }],
        },
        select: {
            id: true,
            mark: true,
            schoolId: true,
            studentId: true,
            exam: {
                select: {
                    id: true,
                    name: true,
                    date: true,
                    subject: {
                        select: {
                            id: true,
                            name: true,
                            code: true,
                        },
                    },
                },
            },
        },
    });
    const studentExam = [];
    allResults.forEach((data) => {
        studentExam.push({
            id: data.id,
            date: data.exam.date,
            subject: data.exam.subject.name,
            mark: data.mark,
        });
    });
    return studentExam;
};
exports.findAllExamOfStudent = findAllExamOfStudent;
const deleteExamData = async (id) => {
    return await db_1.prisma.exam.delete({
        where: {
            id,
        },
    });
};
exports.deleteExamData = deleteExamData;
const deleteExamByIdAndSchoolId = async (id, schoolId) => {
    const curExam = await findExamById(+id);
    if (!curExam) {
        throw new Error('Bad request, exam not found ...');
    }
    if (curExam.schoolId !== schoolId) {
        throw new Error('Un-Authorized');
    }
    return await deleteExamData(+id);
};
exports.deleteExamByIdAndSchoolId = deleteExamByIdAndSchoolId;
const updateExamData = async (id, exam) => {
    return await db_1.prisma.exam.update({
        where: {
            id,
        },
        data: exam,
    });
};
exports.updateExamData = updateExamData;
const updateExamByIdAndSchoolId = async (id, schoolId, exam) => {
    const subjectId = exam.subjectId;
    await findExamByIdSubjectIdAndSchoolId(+id, +subjectId, +schoolId);
    const examData = {
        name: exam.name,
        date: exam.date,
        subjectId: exam.subjectId,
    };
    return await updateExamData(+id, examData);
};
exports.updateExamByIdAndSchoolId = updateExamByIdAndSchoolId;
// create
const createExamData = async (exam) => {
    return await db_1.prisma.exam.create({
        data: exam,
    });
};
exports.createExamData = createExamData;
const createExam = async (exam) => {
    const findSubject = await (0, subject_1.findSubjectById)(+exam.subjectId);
    if (!findSubject) {
        throw new Error('❗️Bad request, subjectId not found ...');
    }
    if (findSubject.schoolId !== exam.schoolId) {
        throw new Error('Un-Authorized(cant assign this subject) ...');
    }
    return await createExamData(exam);
};
exports.createExam = createExam;
