"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.findExpenseByIdAndSchoolId = exports.findExpensesBySchoolIdPagination = exports.findAllExpenses = exports.deleteExpenseById = exports.updateExpenseById = exports.createExpense = exports.findExpenseById = exports.findExpensesBySchoolId = void 0;
const db_1 = require("../prisma/db");
const configs_1 = require("../configs");
const student_1 = require("./student");
const teacher_1 = require("./teacher");
const findAllExpenses = async () => {
    return await db_1.prisma.expense.findMany();
};
exports.findAllExpenses = findAllExpenses;
const findExpensesBySchoolId = async (schoolId) => {
    return await db_1.prisma.expense.findMany({
        where: {
            schoolId,
        },
    });
};
exports.findExpensesBySchoolId = findExpensesBySchoolId;
const findExpenseById = async (id) => {
    return await db_1.prisma.expense.findUnique({
        where: {
            id,
        },
    });
};
exports.findExpenseById = findExpenseById;
const createExpense = async (expense) => {
    if (expense.studentId) {
        const student = await (0, student_1.findStudentById)(expense.studentId);
        if (!student || student.schoolId !== expense.schoolId) {
            throw new Error('student not found');
        }
    }
    if (expense.teacherId) {
        const teacher = await (0, teacher_1.findTeacherById)(expense.teacherId);
        if (!teacher || teacher.schoolId !== expense.schoolId) {
            throw new Error('teacher not found');
        }
    }
    return await db_1.prisma.expense.create({
        data: expense,
    });
};
exports.createExpense = createExpense;
const updateExpenseById = async (id, expense) => {
    if (expense.studentId) {
        const student = await (0, student_1.findStudentById)(expense.studentId);
        if (!student || student.schoolId !== expense.schoolId) {
            throw new Error('student not found');
        }
    }
    if (expense.teacherId) {
        const teacher = await (0, teacher_1.findTeacherById)(expense.teacherId);
        if (!teacher || teacher.schoolId !== expense.schoolId) {
            throw new Error('teacher not found');
        }
    }
    return await db_1.prisma.expense.update({
        where: {
            id,
        },
        data: expense,
    });
};
exports.updateExpenseById = updateExpenseById;
const deleteExpenseById = async (id) => {
    return await db_1.prisma.expense.delete({
        where: {
            id,
        },
    });
};
exports.deleteExpenseById = deleteExpenseById;
const findExpenseByIdAndSchoolId = async (id, schoolId) => {
    const existingExpense = await findExpenseById(+id);
    if (!existingExpense) {
        throw new Error('expense not found');
    }
    if (existingExpense.schoolId !== schoolId) {
        throw new Error('Unauthorized');
    }
    return existingExpense;
};
exports.findExpenseByIdAndSchoolId = findExpenseByIdAndSchoolId;
// Pagination for expenses by schoolId Example
const findExpensesBySchoolIdPagination = async (schoolId, page = 1, limit = 10) => {
    // Check page or limit is not negative or not zero or not a number
    if (page < 1 || limit < 1) {
        return [];
    }
    if (isNaN(page) || isNaN(limit)) {
        return [];
    }
    // Avoid Max Limit
    if (limit > configs_1.MAX_LIMIT) {
        limit = configs_1.MAX_LIMIT;
    }
    return await db_1.prisma.expense.findMany({
        where: {
            schoolId,
        },
        skip: (page - 1) * limit,
        take: limit,
    });
};
exports.findExpensesBySchoolIdPagination = findExpensesBySchoolIdPagination;
