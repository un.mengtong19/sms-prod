"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteSetting = exports.updateSetting = exports.createSetting = exports.findAllSettings = exports.findSettingBySchoolId = exports.findSettingById = void 0;
const db_1 = require("../prisma/db");
const findSettingById = async (id) => {
    return await db_1.prisma.setting.findUnique({
        where: {
            id,
        },
    });
};
exports.findSettingById = findSettingById;
const findSettingBySchoolId = async (schoolId) => {
    return await db_1.prisma.setting.findMany({
        where: {
            schoolId,
        },
    });
};
exports.findSettingBySchoolId = findSettingBySchoolId;
const findAllSettings = async () => {
    return await db_1.prisma.setting.findMany();
};
exports.findAllSettings = findAllSettings;
const createSetting = async (data) => {
    return await db_1.prisma.setting.create({
        data,
    });
};
exports.createSetting = createSetting;
const updateSetting = async (id, data) => {
    const existingSetting = await findSettingById(+id);
    if (!existingSetting) {
        throw new Error("Bad request, setting not found ...");
    }
    return await db_1.prisma.setting.update({
        where: {
            id,
        },
        data,
    });
};
exports.updateSetting = updateSetting;
const deleteSetting = async (id) => {
    const existingSetting = await findSettingById(+id);
    if (!existingSetting) {
        throw new Error("Bad request, setting not found ...");
    }
    return await db_1.prisma.setting.delete({
        where: {
            id,
        },
    });
};
exports.deleteSetting = deleteSetting;
