"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.findResultsByTeacher = exports.createResult = exports.updateResult = exports.deleteResult = exports.findAllResults = exports.findResultByStudentId = exports.findResultById = void 0;
const db_1 = require("../prisma/db");
const findResultById = async (id) => {
    return await db_1.prisma.result.findUnique({
        where: {
            id,
        },
    });
};
exports.findResultById = findResultById;
const findResultByStudentId = async (studentId) => {
    return await db_1.prisma.result.findMany({
        where: {
            studentId,
        },
        select: {
            id: true,
            exam: {
                select: {
                    subject: true,
                    date: true,
                },
            },
            mark: true,
        },
    });
};
exports.findResultByStudentId = findResultByStudentId;
const findAllResults = async (schoolId) => {
    return await db_1.prisma.result.findMany({
        where: {
            schoolId,
        },
    });
};
exports.findAllResults = findAllResults;
const deleteResult = async (id) => {
    return await db_1.prisma.result.delete({
        where: {
            id,
        },
    });
};
exports.deleteResult = deleteResult;
const updateResult = async (id, data) => {
    return await db_1.prisma.result.update({
        where: {
            id,
        },
        data,
    });
};
exports.updateResult = updateResult;
const createResult = async (data) => {
    return await db_1.prisma.result.create({
        data,
    });
};
exports.createResult = createResult;
const findResultsByTeacher = async (teacherId) => {
    const subjects = await db_1.prisma.subject.findMany({
        where: {
            teacherId,
        },
        select: {
            Exam: {
                select: {
                    id: true,
                    Result: {
                        select: {
                            id: true,
                            mark: true,
                            student: true,
                        },
                    },
                },
            },
        },
    });
    const results = [];
    subjects.forEach((subject) => {
        subject.Exam.forEach((exam) => {
            exam.Result.forEach((result) => {
                results.push(result);
            });
        });
    });
    return results;
};
exports.findResultsByTeacher = findResultsByTeacher;
