"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.revokeTokens = exports.deleteRefreshToken = exports.findRefreshTokenById = exports.addRefreshTokenToWhitelist = void 0;
const jwt_1 = require("../utils/jwt");
const db_1 = require("../prisma/db");
// used when we create a refresh token.
function addRefreshTokenToWhitelist({ jti, refreshToken, userId }) {
    return db_1.prisma.refreshToken.create({
        data: {
            id: jti,
            hashedToken: (0, jwt_1.hashToken)(refreshToken),
            userId,
        },
    });
}
exports.addRefreshTokenToWhitelist = addRefreshTokenToWhitelist;
// used to check if the token sent by the client is in the database.
function findRefreshTokenById(id) {
    return db_1.prisma.refreshToken.findUnique({
        where: {
            id,
        },
    });
}
exports.findRefreshTokenById = findRefreshTokenById;
// soft delete tokens after usage.
function deleteRefreshToken(id) {
    return db_1.prisma.refreshToken.update({
        where: {
            id,
        },
        data: {
            revoked: true,
        },
    });
}
exports.deleteRefreshToken = deleteRefreshToken;
function revokeTokens(userId) {
    return db_1.prisma.refreshToken.updateMany({
        where: {
            userId,
        },
        data: {
            revoked: true,
        },
    });
}
exports.revokeTokens = revokeTokens;
