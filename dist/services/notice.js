"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteNoticeById = exports.updateNoticeById = exports.createNotice = exports.findAllNotices = exports.findNoticeById = void 0;
const db_1 = require("../prisma/db");
const findNoticeById = async (id) => {
    return await db_1.prisma.notice.findUnique({
        where: {
            id,
        },
    });
};
exports.findNoticeById = findNoticeById;
const findAllNotices = async () => {
    return await db_1.prisma.notice.findMany();
};
exports.findAllNotices = findAllNotices;
const createNotice = async (notice) => {
    return await db_1.prisma.notice.create({
        data: notice,
    });
};
exports.createNotice = createNotice;
const updateNoticeById = async (id, notice) => {
    return await db_1.prisma.notice.update({
        where: {
            id,
        },
        data: notice,
    });
};
exports.updateNoticeById = updateNoticeById;
const deleteNoticeById = async (id) => {
    return await db_1.prisma.notice.delete({
        where: {
            id,
        },
    });
};
exports.deleteNoticeById = deleteNoticeById;
