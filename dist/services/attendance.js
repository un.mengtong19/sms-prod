"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.findAttendanceBySchoolId = exports.updateAttendanceById = exports.createAttendance = exports.findAttendanceById = exports.deleteAttendanceById = exports.getAllAttendanceOfOneStudentById = void 0;
const db_1 = require("../prisma/db");
const findAttendanceById = async (id) => {
    return await db_1.prisma.attendance.findUnique({
        where: {
            id,
        },
    });
};
exports.findAttendanceById = findAttendanceById;
const createAttendance = async (attendance) => {
    return await db_1.prisma.attendance.create({
        data: attendance,
    });
};
exports.createAttendance = createAttendance;
const updateAttendanceById = async (id, attendance) => {
    return await db_1.prisma.attendance.update({
        where: {
            id,
        },
        data: attendance,
    });
};
exports.updateAttendanceById = updateAttendanceById;
const findAttendanceBySchoolId = async (id) => {
    return await db_1.prisma.attendance.findMany({
        where: {
            schoolId: id,
        },
    });
};
exports.findAttendanceBySchoolId = findAttendanceBySchoolId;
const deleteAttendanceById = async (id) => {
    return await db_1.prisma.attendance.delete({
        where: {
            id,
        },
    });
};
exports.deleteAttendanceById = deleteAttendanceById;
const getAllAttendanceOfOneStudentById = async (id) => {
    return await db_1.prisma.student.findUnique({
        where: {
            id,
        },
        select: {
            Attendance: {
                select: {
                    id: true,
                    date: true,
                    description: true,
                    attendanceType: true,
                    Subject: {
                        select: {
                            id: true,
                            name: true,
                        },
                    },
                },
            },
        },
    });
};
exports.getAllAttendanceOfOneStudentById = getAllAttendanceOfOneStudentById;
