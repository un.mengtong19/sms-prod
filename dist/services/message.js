"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteMessage = exports.findAllMessages = exports.findMessagesByUserId = exports.updateMessage = exports.createMessage = exports.findMessageById = void 0;
const db_1 = require("../prisma/db");
const findMessageById = async (id) => {
    return await db_1.prisma.message.findUnique({
        where: {
            id,
        },
    });
};
exports.findMessageById = findMessageById;
const findMessagesByUserId = async (userId) => {
    return await db_1.prisma.message.findMany({
        where: {
            userId,
        },
    });
};
exports.findMessagesByUserId = findMessagesByUserId;
const findAllMessages = async () => {
    return await db_1.prisma.message.findMany();
};
exports.findAllMessages = findAllMessages;
const createMessage = async (data) => {
    return await db_1.prisma.message.create({
        data,
    });
};
exports.createMessage = createMessage;
const updateMessage = async (id, data) => {
    return await db_1.prisma.message.update({
        where: {
            id,
        },
        data,
    });
};
exports.updateMessage = updateMessage;
const deleteMessage = async (id) => {
    return await db_1.prisma.message.delete({
        where: {
            id,
        },
    });
};
exports.deleteMessage = deleteMessage;
