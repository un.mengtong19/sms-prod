"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteSubjectByIdAndSchoolId = exports.deleteSubjectData = exports.updateSubjectByIdAndSchoolId = exports.updateSubjectById = exports.createSubjectBySchool = exports.createSubjectData = exports.findSubjectByIdClassIdTeacherIdAndSchoolId = exports.findAllSubjectOfTeacherIdByTeacherIdAndSchoolId = exports.findSubjectsBySchoolId = exports.findAllSubjects = exports.findSubjectByName = exports.findSubjectById = void 0;
const db_1 = require("../prisma/db");
const class_1 = require("./class");
const teacher_1 = require("./teacher");
// -- find
const findAllSubjects = async () => {
    return await db_1.prisma.subject.findMany();
};
exports.findAllSubjects = findAllSubjects;
const findSubjectById = async (id) => {
    return await db_1.prisma.subject.findUnique({
        where: {
            id,
        },
    });
};
exports.findSubjectById = findSubjectById;
const findSubjectByName = async (name) => {
    return await db_1.prisma.subject.findMany({
        where: {
            name,
        },
    });
};
exports.findSubjectByName = findSubjectByName;
const findSubjectsBySchoolId = async (schoolId) => {
    return await db_1.prisma.subject.findMany({
        where: {
            schoolId,
        },
        include: {
            Class: {
                select: {
                    schoolId: true,
                },
            },
            Teacher: {
                select: {
                    schoolId: true,
                },
            },
        },
    });
};
exports.findSubjectsBySchoolId = findSubjectsBySchoolId;
const findSubjectByIdClassIdTeacherIdAndSchoolId = async (id, classId, teacherId, schoolId) => {
    // check subjectId input
    const existingSubject = await findSubjectById(+id);
    if (!existingSubject) {
        throw new Error('Bad request, subject not found ...');
    }
    if (existingSubject.schoolId !== schoolId) {
        throw new Error('Un-Authorized');
    }
    // check classId input
    const classD = await (0, class_1.findClassById)(+classId);
    if (!classD) {
        throw new Error('Bad request, class not found ...');
    }
    if (classD.schoolId !== schoolId) {
        throw new Error('Un-Authorized(ClassId)');
    }
    // check teacherId input
    const teacher = await (0, teacher_1.findTeacherById)(+teacherId);
    if (!teacher) {
        throw new Error('Bad request, teacher not found ...');
    }
    if (teacher.schoolId !== schoolId) {
        throw new Error('Un-Authorized(TeacherId)');
    }
    return existingSubject;
};
exports.findSubjectByIdClassIdTeacherIdAndSchoolId = findSubjectByIdClassIdTeacherIdAndSchoolId;
const findAllSubjectOfTeacherIdByTeacherIdAndSchoolId = async (teacherId, schoolId) => {
    const data = await db_1.prisma.subject.findMany({
        where: {
            AND: [{ teacherId }, { schoolId }],
        },
        select: {
            Class: {
                select: {
                    name: true,
                },
            },
            code: true,
            name: true,
            id: true,
        },
    });
    const teacherSubjects = [];
    data.forEach((d) => {
        teacherSubjects.push({
            className: d.Class.name,
            name: d.name,
            id: d.id,
            code: d.code,
        });
    });
    return teacherSubjects;
};
exports.findAllSubjectOfTeacherIdByTeacherIdAndSchoolId = findAllSubjectOfTeacherIdByTeacherIdAndSchoolId;
// -- create
const createSubjectData = async (subject) => {
    return await db_1.prisma.subject.create({
        data: subject,
    });
};
exports.createSubjectData = createSubjectData;
const createSubjectBySchool = async (subject, schoolId) => {
    const teacherId = subject.teacherId;
    const curTeacher = await (0, teacher_1.findTeacherById)(+teacherId);
    if (!curTeacher) {
        throw new Error('Bad request, teacher not found ...');
    }
    if (curTeacher.schoolId !== schoolId) {
        throw new Error('Bad request, teacherId found but not belong to our school ...');
    }
    const classId = subject.classId;
    const curClass = await (0, class_1.findClassById)(+classId);
    if (!curClass) {
        throw new Error('Bad request, class not found ...');
    }
    if (curClass.schoolId !== schoolId) {
        throw new Error('Bad request, classId found but not belong to our school ...');
    }
    const subjectName = subject.name;
    const subOfSchool = await db_1.prisma.subject.findMany({
        where: {
            AND: [{ teacherId }, { classId }, { schoolId: schoolId }],
        },
    });
    const existingNameSub = subOfSchool.find((subjectData) => subjectData.name === subjectName);
    if (existingNameSub) {
        throw new Error('Bad request, Subject already exist ...)');
    }
    return await createSubjectData(subject);
};
exports.createSubjectBySchool = createSubjectBySchool;
// -- update
const updateSubjectByIdAndSchoolId = async (id, schoolId, subject) => {
    const teacherId = subject.teacherId;
    const classId = subject.classId;
    await findSubjectByIdClassIdTeacherIdAndSchoolId(id, classId, teacherId, schoolId);
    // check subject cant have same for 3 properties(name, classId, teacherId)
    const subjectName = subject.name;
    const subOfSchool = await db_1.prisma.subject.findMany({
        where: {
            AND: [{ teacherId }, { classId }, { schoolId: schoolId }],
        },
    });
    const existingNameSub = subOfSchool.find((subjectData) => subjectData.name === subjectName);
    if (existingNameSub) {
        throw new Error('Bad request, Subject already exist ...)');
    }
    return await updateSubjectById(+id, subject);
};
exports.updateSubjectByIdAndSchoolId = updateSubjectByIdAndSchoolId;
const updateSubjectById = async (id, subject) => {
    return await db_1.prisma.subject.update({
        where: {
            id,
        },
        data: subject,
    });
};
exports.updateSubjectById = updateSubjectById;
// -- delete
const deleteSubjectData = async (id) => {
    return await db_1.prisma.subject.delete({
        where: {
            id,
        },
    });
};
exports.deleteSubjectData = deleteSubjectData;
const deleteSubjectByIdAndSchoolId = async (id, schoolId) => {
    const existingSubject = await findSubjectById(+id);
    if (!existingSubject) {
        throw new Error("Bad request, subject not found ...");
    }
    if (existingSubject.schoolId !== schoolId) {
        throw new Error("Un-Authorized ");
    }
    return await deleteSubjectData(+id);
};
exports.deleteSubjectByIdAndSchoolId = deleteSubjectByIdAndSchoolId;
