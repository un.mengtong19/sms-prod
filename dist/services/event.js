"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteEventById = exports.updateEvent = exports.createEvent = exports.findAllEvents = exports.findEventByStudentId = exports.findEventByTeacherId = exports.findEventById = void 0;
const db_1 = require("../prisma/db");
const findEventById = async (id) => {
    return await db_1.prisma.event.findUnique({
        where: {
            id,
        },
    });
};
exports.findEventById = findEventById;
const findAllEvents = async () => {
    return await db_1.prisma.event.findMany();
};
exports.findAllEvents = findAllEvents;
const findEventByTeacherId = async (teacherId) => {
    return await db_1.prisma.event.findMany({
        where: {
            teacherId,
        },
    });
};
exports.findEventByTeacherId = findEventByTeacherId;
const findEventByStudentId = async (studentId) => {
    return await db_1.prisma.event.findMany({
        where: {
            studentId,
        },
    });
};
exports.findEventByStudentId = findEventByStudentId;
const createEvent = async (event) => {
    return await db_1.prisma.event.create({
        data: event,
    });
};
exports.createEvent = createEvent;
const updateEvent = async (event) => {
    await db_1.prisma.event.update({
        where: {
            id: event.id,
        },
        data: event,
    });
};
exports.updateEvent = updateEvent;
const deleteEventById = async (id) => {
    return await db_1.prisma.event.delete({
        where: {
            id,
        },
    });
};
exports.deleteEventById = deleteEventById;
