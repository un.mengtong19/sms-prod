"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const cors_1 = __importDefault(require("cors"));
const body_parser_1 = __importDefault(require("body-parser"));
const routes_1 = __importDefault(require("./routes/routes"));
const swagger_ui_express_1 = __importDefault(require("swagger-ui-express"));
const swagger_json_1 = __importDefault(require("./docs/swagger.json"));
const app = (0, express_1.default)();
const db_1 = require("./prisma/db");
/**
 * App Configuration
 */
app.use((0, cors_1.default)());
app.use(body_parser_1.default.json());
app.use(body_parser_1.default.urlencoded({ extended: true }));
app.use(routes_1.default);
// Serves images
app.use(express_1.default.static('public'));
app.get('/', (req, res) => {
    res.json({ status: 'OK', message: 'API is running on /api', version: '1.0.0' });
});
app.get('/health', (req, res) => {
    const dt = db_1.prisma.school.findFirst();
    if (!dt) {
        res.status(500).json({
            status: {
                message: 'Database is not connected',
            },
        });
    }
    res.json({
        status: {
            message: 'Database is connected',
        },
    });
});
app.use('/api-docs', swagger_ui_express_1.default.serve, swagger_ui_express_1.default.setup(swagger_json_1.default));
app.get('/api-docs', (req, res) => {
    res.json({
        swagger: 'the API documentation is available on /api-docs',
    });
});
/* eslint-disable */
app.use((err, req, res, next) => {
    // @ts-ignore
    if (err && err.name === 'UnauthorizedError') {
        return res.status(401).json({
            status: 'error',
            message: 'missing authorization credentials',
        });
        // @ts-ignore
    }
    else if (err && err.errorCode) {
        // @ts-ignore
        res.status(err.errorCode).json(err.message);
    }
    else if (err) {
        res.status(500).json(err.message);
    }
});
/**
 * Server activation
 */
const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
    console.info(`server up on port ${PORT}`);
});
