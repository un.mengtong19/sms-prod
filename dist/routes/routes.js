"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const user_1 = __importDefault(require("../controllers/user"));
const auth_1 = __importDefault(require("../controllers/auth"));
const message_1 = __importDefault(require("../controllers/message"));
const event_1 = __importDefault(require("../controllers/event"));
const exam_1 = __importDefault(require("../controllers/exam"));
const student_1 = __importDefault(require("../controllers/student"));
const result_1 = __importDefault(require("../controllers/result"));
const subject_1 = __importDefault(require("../controllers/subject"));
const teacher_1 = __importDefault(require("../controllers/teacher"));
const school_1 = __importDefault(require("../controllers/school"));
const attendance_1 = __importDefault(require("../controllers/attendance"));
const setting_1 = __importDefault(require("../controllers/setting"));
const class_1 = __importDefault(require("../controllers/class"));
const payment_1 = __importDefault(require("../controllers/payment"));
const expense_1 = __importDefault(require("../controllers/expense"));
const api = (0, express_1.Router)()
    .use(auth_1.default)
    .use(user_1.default)
    .use(message_1.default)
    .use(event_1.default)
    .use(exam_1.default)
    .use(student_1.default)
    .use(result_1.default)
    .use(subject_1.default)
    .use(teacher_1.default)
    .use(attendance_1.default)
    .use(school_1.default)
    .use(setting_1.default)
    .use(class_1.default)
    .use(payment_1.default)
    .use(expense_1.default);
exports.default = (0, express_1.Router)().use('/api', api);
